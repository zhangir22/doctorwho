﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CardPatient.Models;
namespace CardPatient.Context
{
    public class UserContext:DbContext
    {
        public UserContext()
            : base("name=Context") { }

        public DbSet<User> Users { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CardPatient.Models
{
    [Table("HistoryVisit")]
    public class History
    {
        public int id { get; set; }
        [Required]
        [Display(Name ="Спецализация")]
        public string Specialization { get; set; }
        [Required]
        [Display(Name = "Ф.И.О.")]
        public string FullNameDoctor { get; set; }
        [Required]
        [Display(Name = "Диагноз")]
        public string Diagnosis { get; set; }
        [Required]
        [Display(Name = "Жалобы")]
        public string Complaints { get; set; }
        [Required]
        [Display(Name = "Дата Посещения")]
        public DateTime DateVisit { get; set; } = DateTime.Now;
        public int idPatient { get; set; }
        
       
    }
}
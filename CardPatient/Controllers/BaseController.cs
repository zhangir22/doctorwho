﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CardPatient.Models;
using CardPatient.Context;


namespace CardPatient.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        
        CardContext dbCard;
        HistoryContext dbHistory;
        private string lastIIN;
        // GET: Base
        public BaseController()
        {
            dbCard = new CardContext();
            dbHistory = new HistoryContext();
        }
    

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Card card)
        {
            Card cardPatient = null;
            if (ModelState.IsValid)
            {
                dbCard.Cards.Add(card);
                lastIIN = card.IIN;
                dbCard.SaveChanges();
                cardPatient = card;
            }
            return RedirectToAction("HistoryVisit");
        }
        public ActionResult HistoryVisit()
        {
            History history = new History();
            history.FullNameDoctor = User.Identity.Name;
            history.DateVisit = DateTime.Now;
            return View(history);
        }
        private int GetLastIdList(List<Card>db) 
        {
            return db.LastOrDefault().id;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HistoryVisit(History history)
        {
            List<Card> obj;
            using (CardContext db = new CardContext())
            {
                obj = db.Cards.ToList();
            }
            history.idPatient = GetLastIdList(obj);
            History historyPatient = null;
            if (ModelState.IsValid)
            {
                dbHistory.Histories.Add(history);
                dbHistory.SaveChanges();
                historyPatient = history;
            }
            return View("DoctorsPatient");
        }
        private IOrderedEnumerable<Card> SortPatientsName(List<Card> cards)
        {
            return from u in cards
                   orderby u.FullName
                   select u; ;
        }
        private List<Card> GetPatients()
        {
            List<Card> cards = new List<Card>();
            using(CardContext db = new CardContext())
            {
                var temp = SortPatientsName(db.Cards.ToList());
                foreach(var item in temp)
                {
                    cards.Add(item);
                }
            }
            return cards;
        }
        public ActionResult Patients()
        {
            return View(GetPatients());
        }
    }
}

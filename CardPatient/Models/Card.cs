﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CardPatient.Models
{
    [Table("CardsPatient")]
    public class Card
    {
        public int id { get; set; }
        [Required]
        [Display(Name = "ИИН")]
        public string IIN { get; set; }
        [Required]
        [Display(Name = "Ф.И.О.")]
        public string FullName { get; set; }
        [Required]
        [Display(Name = "Адрес")]
        public string Address { get; set; }
        [Required]
        [Display(Name = "Телефон")]
        public string Phone { get; set; }

    }
}
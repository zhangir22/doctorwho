﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CardPatient.Models;
using CardPatient.Context;
using System.Web.Security;

namespace CardPatient.Controllers
{
    
    public class AccountController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel login)
        {
            if (ModelState.IsValid) {
                User user = null;
                using (UserContext db = new UserContext())
                {
                    user = db.Users.FirstOrDefault(u => u.Email == login.Name && u.Password == login.Password);

                }
                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(login.Name, true);
                    return RedirectToAction("Index", "Base");
                }
                else
                {
                    ModelState.AddModelError("", "Пользователя с такими данными не существует");
                }
               
            }
            return View(login);
        }
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel register)
        {
            if (ModelState.IsValid)
            {
                User user = null;
                using (UserContext db = new UserContext())
                {
                    user = db.Users.FirstOrDefault(u => u.Email == register.Name);
                }
                if (user != null)
                {
                    ModelState.AddModelError("", "Пользователь с таким логином уже существует");
                }
                else
                {
                    using (UserContext db = new UserContext())
                    {
                        db.Users.Add(new Models.User { Email = register.Email, Name = register.Name, Password = register.Password });
                        db.SaveChanges();
                        user = db.Users.Where(u => u.Name == register.Name && u.Email == register.Email && u.Password == register.Password).FirstOrDefault();

                    }
                    if(user != null)
                    {
                        FormsAuthentication.SetAuthCookie(register.Name, true);
                        return RedirectToAction("Index", "Base");
                    }
                }

            }
            return View(register);
        }
        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CardPatient.Models;

namespace CardPatient.Context
{
    public class HistoryContext:DbContext
    {
        public HistoryContext()
            : base("name=Context") { }

        public DbSet<History> Histories { get; set; }
    }
}
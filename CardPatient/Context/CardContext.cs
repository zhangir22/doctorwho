﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CardPatient.Models;

namespace CardPatient.Context
{
    public class CardContext:DbContext
    {
        public CardContext()
            :base("name=Context")
        {}
        public DbSet<Card> Cards { get; set; }

        public System.Data.Entity.DbSet<CardPatient.Models.History> Histories { get; set; }
    }
}